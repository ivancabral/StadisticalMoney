package com.programming.aivox.statisticalmoney.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.programming.aivox.statisticalmoney.R;
import com.programming.aivox.statisticalmoney.graphGen.Bar;
import com.programming.aivox.statisticalmoney.graphGen.BarGraph;
import com.programming.aivox.statisticalmoney.models.DataBaseMoney;
import com.programming.aivox.statisticalmoney.models.Money;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class Index extends AppCompatActivity implements View.OnClickListener {
    TextView tvMoney;
    Money money = new Money();
    List<DataBaseMoney> moneyDbList = new ArrayList<DataBaseMoney>();
    Gson gson = new Gson();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvMoney = (TextView) findViewById(R.id.tvMoney);


        SharedPreferences prefs = getSharedPreferences("my_money", MODE_PRIVATE);
        String restoredText = prefs.getString("my_number", null);
        if (restoredText != null) {
            String idMoney = prefs.getString("my_number", "0"); //0 is the default value.


            System.out.println(idMoney);

            getDbMoney(idMoney);
            money.setMoney(moneyDbList.get(moneyDbList.size()-1).money);
            setMoney(moneyDbList.get(moneyDbList.size()-1).money);
        } else {

            //getDbMoney("[{'month':'Mayo','money':122},{'month':'Julio','money':1000}]");
            money.setMoney(0);
            setMoney(0);
        }


        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        System.out.println(day);
        int maxday = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
        System.out.println(maxday);
        int currentPorcent = (100 * day) / maxday;
        ProgressBar pgbar = (ProgressBar) findViewById(R.id.pbIndex);
        pgbar.setProgress(currentPorcent);

        TextView tvContmen = (TextView) findViewById(R.id.tvContmes);
        int contback = maxday - day;
        String contmen = "Faltan " + String.valueOf(contback) + " dias para terminar el mes";
        tvContmen.setText(contmen);


        setGraphic();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Button btIrCompras = (Button) findViewById(R.id.btIrCompras);
        Button btIngMoney = (Button) findViewById(R.id.btIngdinero);
        Button btCalculator = (Button) findViewById(R.id.btCalculator);

        btCalculator.setOnClickListener(this);
        btIrCompras.setOnClickListener(this);
        btIngMoney.setOnClickListener(this);
    }

    private void setMoney(int mone) {
        String mon = String.valueOf(mone);
        mon += " $";
        tvMoney.setText(mon);

    }

    private void setGraphic() {
        ArrayList<Bar> points = new ArrayList<Bar>();

        Bar d0 = new Bar();

        for (int i = 0; i < moneyDbList.size(); i++) {
            d0.setColor(Color.parseColor("#822fff"));
            d0.setName(moneyDbList.get(i).month);
            d0.setValue(moneyDbList.get(i).money);
            points.add(d0);
        }





        BarGraph g = (BarGraph) findViewById(R.id.graph);
        g.setBars(points);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btIrCompras:
                Intent intent = new Intent(getBaseContext(), GoBuy.class);
                startActivityForResult(intent, 2);
                break;
            case R.id.btIngdinero:
                Intent intentIngD = new Intent(getBaseContext(), InsertMoney.class);
                startActivityForResult(intentIngD, 1);
                break;

            case R.id.btCalculator:
                Intent intCalcu = new Intent(getBaseContext(), Calculator.class);
                startActivity(intCalcu);

                break;


        }
    }

    public void savePreferences(int m) {
        SharedPreferences.Editor editor = getSharedPreferences("my_money", MODE_PRIVATE).edit();

        String json = gson.toJson(moneyDbList);
        Log.e("asd", json.toString());
        editor.putString("my_number", json.toString());
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
                    money.setPastMoney(Integer.parseInt(result));
                    setMoney(money.getMoney());
                    savePreferences(money.getMoney());
                    setDbMoney("Julio", 1000);
                    setGraphic();
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
                break;
            case 2:
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("resultBuy");
                    //TODO restar
                    money.setlessMoney(Integer.parseInt(result));
                    setMoney(money.getMoney());
                    savePreferences(money.getMoney());

                    setGraphic();
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
                break;
        }
    }

    public void setDbMoney(String month, int money) {

        moneyDbList.add(new DataBaseMoney(month, money));

        String json = gson.toJson(moneyDbList);

        Log.e("asd", json.toString());

    }

    public void getDbMoney(String json) {


        //Gson gson = new Gson();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        moneyDbList = new ArrayList<>(Arrays.asList(gson.fromJson(json, DataBaseMoney[].class)));


        //moneyDbList = (List<DataBaseMoney>) gson.fromJson(json, DataBaseMoney.class);
        //moneyDbList.add(new DataBaseMoney("asd",123));


        //setDbMoney("Julio", 1000);
        //moneyDbList.get(0).month.toString();

    }
}
