package com.programming.aivox.statisticalmoney.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sandra on 15/05/2016.
 */
public class DataBaseMoney {

    public String month;
    public int money;


    public DataBaseMoney(String month, int money) {
        this.month = month;
        this.money = money;
    }
}
