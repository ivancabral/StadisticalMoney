
package com.programming.aivox.statisticalmoney.activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.programming.aivox.statisticalmoney.R;

import java.util.ArrayList;

public class GoBuy extends Activity implements View.OnClickListener {
    ArrayList<String> items = new ArrayList<>();
    EditText etItem, etPrecio;
    ArrayAdapter<String> itmAdapterlvHist;
    private int carritoCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gobuy);

        etItem = (EditText) findViewById(R.id.editText);
        etPrecio = (EditText) findViewById(R.id.editText2);

        ListView lvHist = (ListView) findViewById(R.id.listView);



        itmAdapterlvHist = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);


        if (lvHist != null) {
            lvHist.setAdapter(itmAdapterlvHist);
        }

        Button btAgr = (Button) findViewById(R.id.btAgregGobuy);
        Button btCancel = (Button) findViewById(R.id.btCancelGobuy);
        Button btTerminar = (Button) findViewById(R.id.btTerminarGobuy);

        btTerminar.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        btAgr.setOnClickListener(this);

        if (lvHist != null) {
            lvHist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long arg3) {
                    view.setSelected(true);

                    AlertDialog alertDialog = new AlertDialog.Builder(GoBuy.this).create();
                    alertDialog.setTitle("Borrar compra");
                    alertDialog.setMessage("¿Esta seguro que desea borrar la compra?");

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    items.remove(position);
                                    itmAdapterlvHist.notifyDataSetChanged();
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();


                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btAgregGobuy:
                items.add(etItem.getText().toString() + "- Precio: " + etPrecio.getText().toString());
                itmAdapterlvHist.notifyDataSetChanged();

                carritoCount += Integer.parseInt(etPrecio.getText().toString());

                etItem.setText("");
                etPrecio.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btCancelGobuy:
                finish();
                break;

            case R.id.btTerminarGobuy:
                Intent returnIntent = new Intent();

                returnIntent.putExtra("resultBuy",String.valueOf(carritoCount));

                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                break;
        }


    }
}
