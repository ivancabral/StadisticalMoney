package com.programming.aivox.statisticalmoney.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.programming.aivox.statisticalmoney.R;
import com.programming.aivox.statisticalmoney.models.DataBaseMoney;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Calculator extends AppCompatActivity implements View.OnClickListener {

    TextView tvDisplay;
    Button btCalcEqual, btCalcPast, btCalcLess, btCalcMult, btCalcDiv, btCalcNine, btCalcEight,
            btCalcSeven, btCalcSix, btCalcFive, btCalcFour, btCalcTre, btCalcTwo, btCalcOne, btCalcCero,
            btCalcClear, btCalcBack;

    private String resulcClick, internalResult = "";

    ArrayList<String> arrayResult = new ArrayList<>();



    private int result;
    private boolean noRepeatComands = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        setButtons();

        tvDisplay = (TextView) findViewById(R.id.tvCalcDisplay);
    }

    private void setButtons() {
        btCalcCero = (Button) findViewById(R.id.btCalCero);
        btCalcOne = (Button) findViewById(R.id.btCalOne);
        btCalcTwo = (Button) findViewById(R.id.btCalTwo);
        btCalcTre = (Button) findViewById(R.id.btCalTre);
        btCalcFour = (Button) findViewById(R.id.btCalFour);
        btCalcFive = (Button) findViewById(R.id.btCalFive);
        btCalcSix = (Button) findViewById(R.id.btCalSix);
        btCalcSeven = (Button) findViewById(R.id.btCalSeven);
        btCalcEight = (Button) findViewById(R.id.btCalEight);
        btCalcNine = (Button) findViewById(R.id.btCalNine);
        btCalcDiv = (Button) findViewById(R.id.btCalcDiv);
        btCalcMult = (Button) findViewById(R.id.btCalcMult);
        btCalcPast = (Button) findViewById(R.id.btCalcPast);
        btCalcLess = (Button) findViewById(R.id.btCalcLess);
        btCalcClear = (Button) findViewById(R.id.btCalcClear);
        btCalcEqual = (Button) findViewById(R.id.btCalEquals);
        btCalcBack = (Button) findViewById(R.id.btCalBack);

        btCalcBack.setOnClickListener(this);
        btCalcCero.setOnClickListener(this);
        btCalcOne.setOnClickListener(this);
        btCalcTwo.setOnClickListener(this);
        btCalcTre.setOnClickListener(this);
        btCalcFour.setOnClickListener(this);
        btCalcFive.setOnClickListener(this);
        btCalcSix.setOnClickListener(this);
        btCalcSeven.setOnClickListener(this);
        btCalcEight.setOnClickListener(this);
        btCalcNine.setOnClickListener(this);
        btCalcDiv.setOnClickListener(this);
        btCalcMult.setOnClickListener(this);
        btCalcPast.setOnClickListener(this);
        btCalcLess.setOnClickListener(this);
        btCalcClear.setOnClickListener(this);
        btCalcEqual.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_info) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btCalCero:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "0";
                internalResult += "0";
                tvDisplay.setText(resulcClick);

                break;
            case R.id.btCalOne:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "1";
                internalResult += "1";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalTwo:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "2";
                internalResult += "2";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalTre:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "3";
                internalResult += "3";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalFour:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "4";
                internalResult += "4";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalFive:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "5";
                internalResult += "5";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalSix:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "6";
                internalResult += "6";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalSeven:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "7";
                internalResult += "7";
                tvDisplay.setText(resulcClick);
                break;
            case R.id.btCalEight:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "8";
                internalResult += "8";
                tvDisplay.setText(resulcClick);
                break;

            case R.id.btCalNine:
                noRepeatComands = false;
                resulcClick = tvDisplay.getText().toString();
                resulcClick += "9";
                internalResult += "9";
                tvDisplay.setText(resulcClick);
                break;

            case R.id.btCalcDiv:
                if (!noRepeatComands) {

                    resulcClick = tvDisplay.getText().toString();
                    resulcClick += "/";
                    tvDisplay.setText(resulcClick);

                    arrayResult.add(internalResult);
                    arrayResult.add("/");
                    internalResult = "";
                    noRepeatComands = true;
                }
                break;

            case R.id.btCalcMult:
                if (!noRepeatComands) {
                    resulcClick = tvDisplay.getText().toString();
                    resulcClick += "x";
                    tvDisplay.setText(resulcClick);
                    arrayResult.add(internalResult);
                    arrayResult.add("x");
                    internalResult = "";
                    noRepeatComands = true;
                }
                break;
            case R.id.btCalcPast:
                if (!noRepeatComands) {
                    resulcClick = tvDisplay.getText().toString();
                    resulcClick += "+";
                    tvDisplay.setText(resulcClick);
                    arrayResult.add(internalResult);
                    arrayResult.add("+");
                    internalResult = "";
                    noRepeatComands = true;
                }
                break;
            case R.id.btCalcLess:
                if (!noRepeatComands) {
                    resulcClick = tvDisplay.getText().toString();
                    resulcClick += "-";
                    tvDisplay.setText(resulcClick);
                    arrayResult.add(internalResult);
                    arrayResult.add("-");
                    internalResult = "";
                    noRepeatComands = true;
                }
                break;
            case R.id.btCalcClear:
                tvDisplay.setText("");
                arrayResult.clear();
                internalResult = "";
                break;

            case R.id.btCalBack:
                resulcClick = tvDisplay.getText().toString();
                resulcClick = resulcClick.substring(0, resulcClick.length() - 1);
                internalResult = internalResult.substring(0, internalResult.length() - 1);
                tvDisplay.setText(resulcClick);
                System.out.println(resulcClick);
                System.out.println(internalResult);
                break;

            case R.id.btCalEquals:
                if (!noRepeatComands) {
                    result = 0;
                    arrayResult.add(internalResult);

                    for (int i = 0; i < arrayResult.size(); i++) {


                        switch (arrayResult.get(i)) {
                            case "x":
                                result = Integer.parseInt(arrayResult.get(i - 1)) * Integer.parseInt(arrayResult.get(i + 1));
                                tvDisplay.setText(String.valueOf(result));
                                break;
                            case "/":
                                result = Integer.parseInt(arrayResult.get(i - 1)) / Integer.parseInt(arrayResult.get(i + 1));
                                tvDisplay.setText(String.valueOf(result));
                                break;
                            case "+":
                                System.out.println(arrayResult.get(i));
                                System.out.println(arrayResult.get(i + 1));
                                System.out.println(arrayResult.get(i - 1));
                                result = Integer.parseInt(arrayResult.get(i - 1)) + Integer.parseInt(arrayResult.get(i + 1));

                                tvDisplay.setText(String.valueOf(result));

                                break;
                            case "-":
                                result = Integer.parseInt(arrayResult.get(i - 1)) - Integer.parseInt(arrayResult.get(i + 1));
                                tvDisplay.setText(String.valueOf(result));
                                break;

                        }

                        internalResult = String.valueOf(result);


                    }
                }
                break;
        }
    }
}
