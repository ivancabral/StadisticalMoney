package com.programming.aivox.statisticalmoney.models;

/**
 * Created by Sandra on 14/05/2016.
 */
public class Money {
    private int money;

    public void setMoney(int money) {
        this.money = money;
    }

    public void setPastMoney(int money){ this.money += money;}

    public int getMoney() {
        return money;
    }

    public void setlessMoney(int money) {
        this.money -= money;
    }
}
