package com.programming.aivox.statisticalmoney.activitys;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.programming.aivox.statisticalmoney.R;
import com.programming.aivox.statisticalmoney.models.Money;

public class InsertMoney extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_money);

        final EditText etInsmon = (EditText) findViewById(R.id.etInsmon);

        Button btInsmon = (Button) findViewById(R.id.btInsmon);

        if (btInsmon != null) {
            btInsmon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    if (etInsmon != null) {
                        returnIntent.putExtra("result",etInsmon.getText().toString());
                    }
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();

                }
            });
        }
    }
}
